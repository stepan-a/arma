\begin{frame}
  \frametitle{First order moving average model}
  \framesubtitle{Definition}

  \begin{block}{MA(1) model}
    Let $\{\epsilon_t, t\in\mathbb Z\}$ be a univariate Gaussian white
    noise with 0 mean and variance $\sigma_{\epsilon}^2$. The
    stochastic process $\{y_t,t\in\mathbb Z\}$ is a gaussian first order
    moving average process, $\{y_t\}\sim MA(1)$, if
    \begin{equation}\label{eq:ma:1:def}\tag{MA-1}
    y_t = \mu +  \epsilon_t + \theta \epsilon_{t-1}
    \end{equation}
    with $\mu\in\mathbb R$ and $\theta\in (-1,1)$.
  \end{block}
  \begin{itemize}
  \item The joint distribution of $(y_t, y_{t-1}, \dots, y_{t-n})$ is
    time invariant \newline$\Rightarrow$ \eqref{eq:ma:1:def} defines a stationary
    stochastic process.
  \item The main property of this model is that the structure of
    dependency is limited.
  \item $y_t$ and $y_{t-i}$, for $i>1$ are independent because they do
    not have any innovation, $\epsilon$, in common.
  \item[$\Rightarrow$] $\gamma_y(0)$ and $\gamma_y(1)$ are non zero,
    but $\gamma_y(h)=0$ for all $|h|>1$.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{First order moving average model}
  \framesubtitle{Moments}

  The expectation of a first order moving average process, as defined
  in \ref{eq:ma:1:def}, is $\mu$.\newline

  \bigskip

  \begin{block}{MA(1) autocovariance function}
    The autocovariance function of a first order moving average
    process, as defined in \ref{eq:ma:1:def}, is given by:
    \[
    \begin{split}
      \gamma_y(0) &= (1+\theta^2)\sigma_{\epsilon}^2\\
      \gamma_y(1) &= \theta \sigma_{\epsilon}^2\\
      \gamma_y(h) &= 0\quad \forall |h|>1
    \end{split}
    \]
  \end{block}
\end{frame}

\begin{notes}
  Applying the unconditional expectation to the definition
  \eqref{eq:ma:1:def} of $y_t$, we obtain:
  \[
  \mathbb E \left[y_t\right] = \mu + \mathbb E \left[\epsilon_t\right]
  + \theta\mathbb E \left[\epsilon_{t-1}\right]
  \]
  \[
  \Leftrightarrow\mathbb E \left[y_t\right] = \mu
  \]
  because $\{\epsilon_t, t\in\mathbb Z\}$ is a zero mean stochastic
  process. The variance, or the autocovariance of order 0, of $y_t$ is
  defined by:
  \[
  \mathbb V\left[y_t\right] = \mathbb E\left[(y_t-\mu)^2\right]
  \]
  Substituting the definition of $y_t$ in terms of innovations, we
  obtain:
  \[
  \gamma_y(0) = \mathbb E\left[(\epsilon_t + \theta \epsilon_{t-1})^2\right]
  \]
  \[
  \Leftrightarrow\gamma_y(0) = \mathbb E\left[\epsilon_t^2 + \theta^2
    \epsilon_{t-1}^2 + \theta\epsilon_t\epsilon_{t-1}\right]
  \]
  \[
  \Leftrightarrow\gamma_y(0) = \mathbb E\left[\epsilon_t^2\right] + \theta^2
    \mathbb E\left[\epsilon_{t-1}^2\right]
  \]
  by linearity of the expectation and because $\{\epsilon_t,
  t\in\mathbb Z\}$ is an \textit{i.i.d.} stochastic process (which
  implies that $y_t$ and $y_{t-1}$ are uncorrelated). Finally, we
  have:
  \[
  \gamma_y(0) = (1+\theta^2)\sigma_{\epsilon}^2
  \]
  The autocovariance function is defined as:
  \[
  \gamma_y(h) = \mathbb E\left[(y_t-\mu)(y_{t-h}-\mu)\right]
  \]
  For $h=1$, substituting the definition of $y_t$, we have:
  \[
  \gamma_y(1) = \mathbb E\left[(\epsilon_t+\theta\epsilon_{t-1})(\epsilon_{t-1}+\theta\epsilon_{t-2})\right]
  \]
  \[
  \Leftrightarrow\gamma_y(1) = \mathbb
  E\left[\epsilon_t\epsilon_{t-1}\right]+
  \theta\mathbb E\left[\epsilon_t\epsilon_{t-2}\right]+
  \theta\mathbb E\left[\epsilon_{t-1}\epsilon_{t-1}\right]+
  \theta^2\mathbb E\left[\epsilon_{t-1}\epsilon_{t-2}\right]
  \]
  \[
  \Leftrightarrow\gamma_y(1) = \theta\sigma_{\epsilon}^2
  \]
  because $\{\epsilon_t,
  t\in\mathbb Z\}$ is an \textit{i.i.d.} stochastic process. More
  generally, for any order $h$, we have:
  \[
  \gamma_y(h) = \mathbb
  E\left[\epsilon_t\epsilon_{t-h}\right]+
  \theta\mathbb E\left[\epsilon_t\epsilon_{t-h-1}\right]+
  \theta\mathbb E\left[\epsilon_{t-1}\epsilon_{t-h}\right]+
  \theta^2\mathbb E\left[\epsilon_{t-1}\epsilon_{t-h-1}\right]
  \]
  which is zero for all $|h|>1$.
  \clearpage
\end{notes}


\begin{frame}
  \frametitle{Finite order moving average model}
  \framesubtitle{Definition}

  \begin{block}{MA(q) model}
    Let $\{\epsilon_t, t\in\mathbb Z\}$ be a univariate Gaussian white
    noise with 0 mean and variance $\sigma_{\epsilon}^2$. The
    stochastic process $\{y_t,t\in\mathbb Z\}$ is a $q<\infty$ order
    moving average process, $\{y_t\}\sim MA(q)$, if
    \begin{equation}\label{eq:ma:q:def}\tag{MA-q}
    y_t = \mu +  \epsilon_t + \sum_{i=1}^q\theta_i \epsilon_{t-i}
    \end{equation}
    with $\mu$ and $\theta_i$ $\in\mathbb R$.
  \end{block}
  \begin{itemize}
  \item \eqref{eq:ma:q:def} defines a stationary
    stochastic process.
  \item The main property of this model is that the structure of
    dependency is limited.
  \item $y_t$ and $y_{t-i}$, for $i>q$ are independent because they do
    not have any innovation, $\epsilon$, in common.
  \item Later we will impose some restrictions on the coefficients $\theta_i$.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{First order moving average model}
  \framesubtitle{Moments}

  The expectation of an MA($q$) process, as defined
  in \ref{eq:ma:q:def}, is $\mu$.\newline

  \begin{block}{MA($q$) autocovariance function}
    The autocovariance function of a finite order moving average
    process, as defined in \ref{eq:ma:q:def}, is given by:
    \[
    \begin{split}
      \gamma_y(0) &= \sigma_{\epsilon}^2\sum_{i=1}^q\theta_i^2\\
      \gamma_y(h) &= \sigma_{\epsilon}^2\sum_{i=0}^q\theta_i\theta_{i-h},\quad\forall\ |h|\leq q\\
      \gamma_y(h) &= 0,\quad\forall\ |h|>q
    \end{split}
    \]
    with $\theta_0 = 1$ and $\theta_i=0$ for all $i<0$.
  \end{block}
\end{frame}


\begin{notes}
  By definition of the autocovariance function, we have:
  \[
  \gamma(h) = \mathbb E \left[\left(\sum_{i=0}^q\theta_i\epsilon_{t-i}\right)\left(\sum_{j=0}^q\theta_j\epsilon_{t-h-j}\right)\right]
  \]
  with $\theta_0 = 1$. By linearity of the expectation, we obtain:
  \[
  \gamma(h) = \sum_{i=0}^q\sum_{j=0}^q\theta_i\theta_j\mathbb E \left[\epsilon_{t-i}\epsilon_{t-h-j}\right]
  \]
  Because:
  \[
  \mathbb E \left[\epsilon_{t-i}\epsilon_{t-h-j}\right] =
  \begin{cases}
    \sigma_{\epsilon}^2,\quad &\text{if }t-i=t-h-j\\
    0\quad &\text{ otherwise}
  \end{cases}
  \]
  we know that the expectations are non zero if and only if $j = i-h$,
  so that we can remove the second sum:
  \[
  \gamma(h) = \sigma_{\epsilon}^2\sum_{i=0}^q\theta_i\theta_{i-h}
  \]
  with $\theta_i=0$ for all $i<0$.

\end{notes}

% Local Variables:
% TeX-master: "slides.tex"
% ispell-check-comments: exclusive
% ispell-local-dictionary: "american-insane"
% End: