\begin{frame}
  \frametitle{ARMA  models}
  \framesubtitle{Definition}

  \begin{block}{ARMA(p,q) model}
    Let $\{\epsilon_t, t\in\mathbb Z\}$ be a univariate Gaussian white
    noise with 0 mean and variance $\sigma_{\epsilon}^2$. The
    stochastic process $\{y_t,t\in\mathbb Z\}$ is an ARMA(p,q) process, if
    \begin{equation}\label{eq:arma:pq:def}\tag{ARMA}
    y_t = c +  \sum_{i=1}^p\varphi_{i} y_{t-i} + \epsilon_t + \sum_{i=1}^q\theta_{i} \epsilon_{t-i}
    \end{equation}
    with $c, \{\varphi_i\}_{i=1}^p, \{\theta_i\}_{i=1}^q\in\mathbb R$.
  \end{block}
  \begin{itemize}
  \item Later we will impose some restrictions on the coefficients
    $\varphi_i$ to ensure that the stochastic process is stationary.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{ARMA models}
  \framesubtitle{Moments}

  The unconditional expectation of an ARMA(p,q) stochastic process, as defined
  in \ref{eq:arma:pq:def}, is $\mu =
  \nicefrac{c}{1-\sum_{i=1}^p\varphi_i}$.


  \begin{block}{ARMA(p,q) autocovariance function}
    The autocovariance function of a finite order ARMA
    stochastic process, as defined in \ref{eq:arma:pq:def}, must solve
    the following Yule Walker equations:
    {\small\[
    \gamma_y(h) = \sum_{i=0}^p\varphi_i\gamma_y(h-i) +
    \sum_{j=0}^{q-h}\theta_{j+h}\alpha(j)\quad \forall h=0,1,\dots,n
    \]}
    with
    {\small\[
    \alpha(j) =
    \sum_{i=1}^{\min(i,p)}\varphi_i\alpha(j-i)+\theta_j\sigma_{\epsilon}^2
    \]}
    and $\alpha(0)=\sigma_{\epsilon}^2$, $\theta_j=0$ for all $j>q$,
    and $n=\max(p+1,q)$. The subsequent terms are recursively defined
    by:
    {\small\[
    \gamma_y(h) = \sum_{i=0}^p\varphi_i\gamma_y(h-i), \quad\forall h>n
    \]}
  \end{block}
\end{frame}


\begin{notes}
  For any $h$ we have:
  \[
  \gamma_y(h) = \sum_{i=0}^p\varphi_i\gamma_y(h-i) + \omega(h)
  \]
  with
  \[
  \omega(h) = \sum_{i=0}^q\theta_i\Expectation{y_{t-h}\epsilon_{t-i}}
  \]
  with $\theta_0 = 1$. Because $\Expectation{y_t\epsilon_s}=0$ for all
  $s>t$, we can simply the previous expression:
  \[
  \omega(h) = \sum_{i=h}^q\theta_i\Expectation{y_{t-h}\epsilon_{t-i}}
  \]
  The stochastic process is assumed to be stationary, so that
  $\Expectation{y_t\epsilon_s}=\Expectation{y_{t-l}\epsilon_{s-l}}$
  for any $l\in\mathbb  Z$. Using this property, we can rewrite
  $\omega(h)$ as:
  \[
  \begin{split}
    \omega(h) &= \sum_{i=h}^q\theta_i\Expectation{y_{t}\epsilon_{t+h-i}}\\
    \Leftrightarrow\omega(h) &=\sum_{i=0}^{q-h}\theta_{i+h}\Expectation{y_{t}\epsilon_{t-i}}
  \end{split}
  \]
  Where
  \[
  \Expectation{y_t\epsilon_t} = \sigma_{\epsilon}^2
  \]
  \[
  \begin{split}
    \Expectation{y_t\epsilon_{t-1}} &= \Expectation{\left(\sum_{i=1}^p\varphi_{i} y_{t-i} + \epsilon_t + \sum_{i=1}^q\theta_{i} \epsilon_{t-i}\right)\epsilon_{t-1}}\\
    &= \Expectation{\left(\varphi_1y_{t-1}+\theta_1\epsilon_{t-1}\right)\epsilon_{t-1}}\\
    &= \varphi_1\Expectation{y_{t}\epsilon_{t}}+\theta_1\sigma_{\epsilon}^2\\
    &= \left(\varphi_1+\theta_1\right)\sigma_{\epsilon}^2
  \end{split}
  \]
\smallskip
  \[
  \begin{split}
    \Expectation{y_t\epsilon_{t-2}} &= \Expectation{\left(\sum_{i=1}^p\varphi_{i} y_{t-i} + \epsilon_t + \sum_{i=1}^q\theta_{i} \epsilon_{t-i}\right)\epsilon_{t-2}}\\
    &= \Expectation{\left(\varphi_1y_{t-1}+\varphi_2y_{t-2}+\theta_2\epsilon_{t-2}\right)\epsilon_{t-2}}\\
    &= \varphi_1\Expectation{y_{t}\epsilon_{t-1}}+\left(\varphi_2+\theta_2\right)\sigma_{\epsilon}^2\\
    &= \left(\varphi_1\left(\varphi_1+\theta_1\right)+\varphi_2+\theta_2\right)\sigma_{\epsilon}^2
  \end{split}
  \]
  More generally, define $\alpha(j) =
  \Expectation{y_t\epsilon_{t-j}}$. The function $\alpha(j)$ can be obtained
  recursively:
  \[
  \alpha(j) =
  \sum_{i=1}^{\min(j,p)}\varphi_i\alpha(j-i)+\theta_j\sigma_{\epsilon}^2
  \]
  with $\theta_j=0$ for all $j>q$ and
  $\alpha(0)=\sigma_{\epsilon}^2$. Finally we can rewrite the Yule
  Walker equations as:
  \[
  \gamma_y(h) = \sum_{i=0}^p\varphi_i\gamma_y(h-i) + \omega(h)
  \]
  with
  \[
  \omega(h) = \sum_{i=0}^{q-h}\theta_{i+h}\alpha(i)
  \]
  and
  \[
  \alpha(i) =
  \sum_{j=1}^{\min(j,p)}\varphi_j\alpha(i-j)+\theta_i\sigma_{\epsilon}^2
  \]

\end{notes}


% Local Variables:
% TeX-master: "slides.tex"
% ispell-check-comments: exclusive
% ispell-local-dictionary: "american-insane"
% End: